﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ReleaseTracker.Common
{
  /// <summary>
  /// Represents information which is derived or related to a specific application
  /// </summary>
  public abstract class AppInfo
  {
    public DomainApp App { get; set; }
  }
}
