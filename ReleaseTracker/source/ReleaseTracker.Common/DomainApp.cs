﻿using System;
using System.Collections.Generic;
using System.IO;

namespace ReleaseTracker.Common
{
  public class DomainApp : IEquatable<DomainApp>
  {
    public string Name { get; set; }
    public string GitUrl { get; set; }
    public string OctopusProject { get; set; }
    public string TeamCityBuildType { get; set; }
    public string AlternateTeamCityBuildType { get; set; }
    public HashSet<string> JiraProjects { get; } = new HashSet<string>(StringComparer.OrdinalIgnoreCase);

    /// <summary>
    /// Only Applicable to Torque projects.
    /// </summary>
    public string TorqueEnvironment { get; set; }

    #region Equality

    public override bool Equals(object obj) => Equals(obj as DomainApp);

    public bool Equals(DomainApp other) => other != null && Name == other.Name;

    public override int GetHashCode() => 539060726 + EqualityComparer<string>.Default.GetHashCode(Name);

    public static bool operator ==(DomainApp app1, DomainApp app2) => EqualityComparer<DomainApp>.Default.Equals(app1, app2);

    public static bool operator !=(DomainApp app1, DomainApp app2) => !(app1 == app2);

    #endregion
  }
}
