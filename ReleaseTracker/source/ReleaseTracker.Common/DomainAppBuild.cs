﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ReleaseTracker.Common
{
  public sealed class DomainAppBuild : AppInfo
  {
    public string GitCommit { get; set; }
    public string BuildNumber { get; set; }
    public string TeamCityUrl { get; set; }
    public string OctopusDeployUrl { get; set; }
    public string JqlQuery => $"\"Fixed in Build\" = {BuildNumber} and project in ({string.Join(",", App.JiraProjects)})";

    public DateTimeOffset? ReleasedAt { get; set; }
  }
}
