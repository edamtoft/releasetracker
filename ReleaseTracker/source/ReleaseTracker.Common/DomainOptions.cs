﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ReleaseTracker.Common
{
  public sealed class DomainOptions
  {
    public List<DomainApp> DomainApps { get; set; } = new List<DomainApp>();
  }
}
