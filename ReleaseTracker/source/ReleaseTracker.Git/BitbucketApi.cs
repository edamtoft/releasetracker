﻿using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using ReleaseTracker.Common;
using ReleaseTracker.Git.Models;
using ReleaseTracker.Git.Models.Bitbucket;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Runtime.Caching;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace ReleaseTracker.Git
{
  public sealed class BitbucketApi : ISourceRepo, IDisposable
  {
    private readonly BitbucketApiOptions _options;
    private readonly HttpClient _client;
    private readonly JsonSerializerSettings _serializerSettings;
    private readonly ObjectCache _cache;

    public BitbucketApi(IOptions<BitbucketApiOptions> options)
    {
      _options = options.Value;
      _client = new HttpClient
      {
        DefaultRequestHeaders =
        {
          { "Accept", "application/json" },
          { "Authorization", _options.Authorization }
        }
      };
      _serializerSettings = new JsonSerializerSettings
      {
        ContractResolver = new CamelCasePropertyNamesContractResolver()
      };
      _cache = new MemoryCache("BitbucketApi");
    }

    public void Dispose() => _client.Dispose();

    public async Task<List<AppChange>> ListChangesFromMaster(DomainApp app, string fromCommit, bool includeMerges)
    {
      var endpoint = GetEndpoint(app, $"commits/master?exclude={fromCommit}");
      var commits = await FetchPaginated<Commit>(endpoint);

      return commits
        .Where(commit => includeMerges || commit.Parents.Count == 1)
        .Select(commit => new AppChange
        {
          App = app,
          Author = commit.Author?.User?.DisplayName,
          AuthorEmail = commit.Author?.Raw,
          CommitId = commit.Hash,
          Message = commit.Message,
          When = commit.Date,
        })
        .ToList();
    }

    public async Task<List<AppChange>> ListChanges(DomainApp app, string fromCommit, string toCommit, bool includeMerges)
    {
      if (string.Equals(fromCommit, toCommit, StringComparison.OrdinalIgnoreCase))
      {
        return new List<AppChange>();
      }

      var endpoint = GetEndpoint(app, $"commits/?include={toCommit}&exclude={fromCommit}");
      var commits = await FetchPaginated<Commit>(endpoint);

      return commits
        .Where(commit => includeMerges || commit.Parents.Count == 1)
        .Select(commit => new AppChange
        {
          App = app,
          Author = commit.Author?.User?.DisplayName,
          AuthorEmail = commit.Author?.Raw,
          CommitId = commit.Hash,
          Message = commit.Message,
          When = commit.Date,
        })
        .ToList();
    }

    public async Task<List<(string Name, string Sha1)>> LookupTags(DomainApp app, string commit)
    {
      var byCommit = !string.IsNullOrEmpty(commit);

      var endpoint = GetEndpoint(app, byCommit ? $"refs/tags?q=target.hash=\"{commit}\"" : "refs/tags");
      var tags = await FetchPaginated<Tag>(endpoint);

      return tags
        .Where(tag => !byCommit || tag.Target.Hash == commit)
        .OrderByDescending(tag => tag.Date)
        .Select(tag => (tag.Name, tag.Target.Hash))
        .ToList();
    }

    private async Task<T> Fetch<T>(string api)
    {
      using (var response = await _client.GetAsync(api))
      {
        if (response.StatusCode == HttpStatusCode.NotFound)
        {
          return default(T);
        }

        var responseText = await response.Content.ReadAsStringAsync();

        var paginatedResponse = JsonConvert.DeserializeObject<T>(responseText, _serializerSettings);

        return paginatedResponse;
      }
    }

    private async Task<List<T>> FetchPaginated<T>(string api)
    {
      var cacheKey = $"bitbucket:{api}";

      if (_cache.Get(cacheKey) is string json)
      {
        return JsonConvert.DeserializeObject<List<T>>(json);
      }

      var results = new List<T>();
      var next = api;
      do
      {
        var page = await Fetch<Paginated<T>>(next);

        if (page != null)
        {
          results.AddRange(page.Values);
        }

        if (results.Count > _options.MaxItems)
        {
          throw new Exception("More than the max configured items were returned from the bitbucket API");
        }

        next = page?.Next;
      }
      while (next != null);

      _cache.Add(cacheKey, JsonConvert.SerializeObject(results), DateTimeOffset.Now.AddMinutes(_options.CacheMinutes));

      return results;
    }

    private static string GetEndpoint(DomainApp app, string relativePath)
    {
      var pattern = new StringBuilder()
        .Append(Regex.Escape("https://bitbucket.org/"))
        .Append(@"([a-z\.\-]+)\/([a-z\.\-]+)")
        .Append(Regex.Escape(".git"))
        .ToString();

      var match = Regex.Match(app.GitUrl, pattern);

      if (!match.Success)
      {
        throw new FormatException("Unexpected git repository format for bitbucket API");
      }

      var user = match.Groups[1].Value;
      var repo = match.Groups[2].Value;

      return $"https://api.bitbucket.org/2.0/repositories/{user}/{repo}/{relativePath}";
    }

    public Task UpdateAll()
    {
      throw new NotSupportedException("Update not relevant to UI");
    }

    public async Task<string> GetCommitByTag(DomainApp app, string tag)
    {
      var endpoint = GetEndpoint(app, $"refs/tags/{tag}");
      var response = await Fetch<Tag>(endpoint);
      return response?.Target.Hash;
    }
  }
}
