﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace ReleaseTracker.Git
{
  public class BitbucketApiOptions
  {
    public string Authorization { get; set; }
    public int CacheMinutes { get; set; } = 10;
    public int MaxItems { get; set; } = 100;
  }
}