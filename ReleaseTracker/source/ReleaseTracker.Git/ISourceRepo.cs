﻿using System.Collections.Generic;
using System.Threading.Tasks;
using ReleaseTracker.Common;
using ReleaseTracker.Git.Models;

namespace ReleaseTracker.Git
{
  /// <summary>
  /// Represents an access layer for git repositories from a specific domain application
  /// </summary>
  public interface ISourceRepo
  {
    Task<string> GetCommitByTag(DomainApp app, string tag);
    Task<List<(string Name, string Sha1)>> LookupTags(DomainApp app, string commit = null);
    Task<List<AppChange>> ListChanges(DomainApp app, string fromCommit, string toCommit, bool includeMerges);
    Task<List<AppChange>> ListChangesFromMaster(DomainApp app, string fromCommit, bool includeMerges);
    Task UpdateAll();
  }
}