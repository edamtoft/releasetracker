﻿using ReleaseTracker.Common;
using System;
using System.Collections.Generic;
using System.Text;

namespace ReleaseTracker.Git.Models
{
  /// <summary>
  /// A git commit from a specific application's repository
  /// </summary>
  public sealed class AppChange : AppInfo
  {
    public string CommitId { get; set; }
    public string Author { get; set; }
    public string AuthorEmail { get; set; }
    public string Message { get; set; }
    public DateTimeOffset When { get; set; }
    public bool IsMerge { get; set; }
  }
}
