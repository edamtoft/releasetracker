﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ReleaseTracker.Git.Models.Bitbucket
{
  internal sealed class Author
  {
    public string Raw { get; set; }
    public User User { get; set; }
  }
}
