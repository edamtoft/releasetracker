﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ReleaseTracker.Git.Models.Bitbucket
{
  internal sealed class Commit : CommitInfo
  {
    public Author Author { get; set; }
    public List<CommitInfo> Parents { get; set; }
    public DateTimeOffset Date { get; set; }
    public string Message { get; set; }
  }
}
