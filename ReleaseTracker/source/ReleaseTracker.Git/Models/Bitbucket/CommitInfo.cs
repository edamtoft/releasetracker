﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ReleaseTracker.Git.Models.Bitbucket
{
  internal class CommitInfo
  {
    public string Hash { get; set; }
  }
}
