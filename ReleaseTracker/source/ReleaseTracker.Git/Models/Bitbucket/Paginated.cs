﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace ReleaseTracker.Git.Models.Bitbucket
{
  internal sealed class Paginated<T>
  {
    public List<T> Values { get; set; }
    public string Next { get; set; }
  }
}
