﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace ReleaseTracker.Git.Models.Bitbucket
{
  internal sealed class Tag
  {
    public string Name { get; set; }
    public DateTimeOffset? Date { get; set; }
    public Commit Target { get; set; }
  }
}
