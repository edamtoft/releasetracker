﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ReleaseTracker.Git.Models.Bitbucket
{
  public sealed class User
  {
    public string DisplayName { get; set; }
  }
}
