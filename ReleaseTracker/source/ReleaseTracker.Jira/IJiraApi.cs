﻿using System.Threading.Tasks;
using ReleaseTracker.Jira.Models;

namespace ReleaseTracker.Jira
{
  /// <summary>
  /// Represents an API used to get issue-tracking details based on a linked issue key
  /// </summary>
  public interface IJiraApi
  {
    Task<JiraIssue> GetIssue(string key);

    Task<JiraSearchResult> Search(string jql);
  }
}