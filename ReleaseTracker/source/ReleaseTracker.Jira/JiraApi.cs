﻿using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json.Serialization;
using ReleaseTracker.Jira.Models;
using System;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Runtime.Caching;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace ReleaseTracker.Jira
{
  public sealed class JiraApi : IDisposable, IJiraApi
  {
    private readonly JiraOptions _options;
    private readonly HttpClient _httpClient;
    private readonly MemoryCache _cache = new MemoryCache("Jira");

    public JiraApi(IOptions<JiraOptions> options)
    {
      _options = options.Value;
      _httpClient = new HttpClient
      {
        BaseAddress = new Uri(_options.Endpoint),
        DefaultRequestHeaders =
        {
          { "Accept", "application/json" },
          { "Authorization", _options.Authorization }
        }
      };
    }

    private static JsonSerializerSettings Settings { get; } = new JsonSerializerSettings
    {
      ContractResolver = new CamelCasePropertyNamesContractResolver()
    };

    void IDisposable.Dispose() => _httpClient.Dispose();

    public Task<JiraIssue> GetIssue(string key)
    {
      return GetCached<JiraIssue>($"rest/api/2/issue/{key}");
    }

    private static string GetKey(string endpoint)
    {
      using (var sha1 = SHA1.Create())
      {
        var hashBytes = sha1.ComputeHash(Encoding.Default.GetBytes(endpoint));
        var sb = new StringBuilder(40);
        foreach (var b in hashBytes)
        {
          sb.AppendFormat("{0:x2}", b);
        }
        return sb.ToString();
      }
    }

    private async Task<T> GetCached<T>(string endpoint) where T : class
    {
      var key = GetKey(endpoint);

      if (_cache.Get(key) is T cachedItem)
      {
        return cachedItem;
      }

      using (var response = await _httpClient.GetAsync(endpoint))
      {
        if (response.StatusCode == HttpStatusCode.NotFound)
        {
          return null;
        }

        response.EnsureSuccessStatusCode();

        var responseText = await response.Content.ReadAsStringAsync();

        var deserializedResult = JsonConvert.DeserializeObject<T>(responseText, Settings);

        _cache.Add(key, deserializedResult, DateTimeOffset.Now.AddMinutes(2));

        return deserializedResult;
      }
    }

    public Task<JiraSearchResult> Search(string jql)
    {
      return GetCached<JiraSearchResult>($"rest/api/2/search?jql={HttpUtility.UrlEncode(jql)}&maxResults=200");
    }
  }
}
