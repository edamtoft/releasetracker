﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ReleaseTracker.Jira
{
  public sealed class JiraOptions
  {
    public string Endpoint { get; set; }
    public string Authorization { get; set; }
  }
}
