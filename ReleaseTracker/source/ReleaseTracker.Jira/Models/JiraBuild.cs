﻿using Newtonsoft.Json;

namespace ReleaseTracker.Jira.Models
{
  public sealed class JiraBuild
  {
    public string Name { get; set; }
  }
}