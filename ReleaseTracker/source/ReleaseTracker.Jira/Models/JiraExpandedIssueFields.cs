﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace ReleaseTracker.Jira.Models
{
  public sealed class JiraExpandedIssueFields : JiraIssueFields
  {
    [JsonProperty("issuelinks")]
    public List<JiraIssueLink> IssueLinks { get; set; }

    [JsonProperty("customfield_17907")]
    public JiraBuild Build { get; set; }

    [JsonProperty("customfield_13602")]
    public string ReleaseInstructionTodos { get; set; }

    public JiraProject Project { get; set; }

    [JsonProperty("customfield_10011")]
    public string EpicLink { get; set; }


    [JsonProperty("customfield_10007")]
    public double? StoryPoints { get; set; }
  }
}
