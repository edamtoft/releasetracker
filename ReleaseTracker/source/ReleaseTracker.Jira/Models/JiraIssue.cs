﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ReleaseTracker.Jira.Models
{
  public sealed class JiraIssue : JiraIssue<JiraExpandedIssueFields>
  {
    [JsonIgnore]
    public IEnumerable<JiraIssue<JiraIssueFields>> Dependencies => 
      from link in Fields.IssueLinks ?? Enumerable.Empty<JiraIssueLink>()
      where link.Type.Name == "Dependency" && link.InwardIssue != null
      select link.InwardIssue;
  }

  public sealed class JiraIssueRef : JiraIssue<JiraIssueFields>
  {

  }

  public abstract class JiraIssue<TFields> where TFields : JiraIssueFields
  {
    [JsonProperty("key")]
    public string Key { get; set; }

    [JsonProperty("fields")]
    public TFields Fields { get; set; }

    //[JsonIgnore]
    //public bool IsResolved => new[] { "Pending UAT", "Pending Promotion", "Closed", "Released", "Pending Confirmation" }.Contains(Fields.Status.Name);
  }
}
