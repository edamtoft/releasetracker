﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace ReleaseTracker.Jira.Models
{
  public class JiraIssueFields
  {
    public JiraStatus Status { get; set; }
    public string Summary { get; set; }
  }
}