﻿using Newtonsoft.Json;

namespace ReleaseTracker.Jira.Models
{
  public sealed class JiraIssueLink
  {
    public JiraLinkType Type { get; set; }
    public JiraIssueRef InwardIssue { get; set; }
    public JiraIssueRef OutwardIssue { get; set; }
  }
}