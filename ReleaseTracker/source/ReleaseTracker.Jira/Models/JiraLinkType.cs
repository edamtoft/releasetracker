﻿using Newtonsoft.Json;

namespace ReleaseTracker.Jira.Models
{
  public sealed class JiraLinkType
  {
    public string Name { get; set; }
    public string Inward { get; set; }
    public string Outward { get; set; }
  }
}