﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ReleaseTracker.Jira.Models
{
  public sealed class JiraProject
  {
    public string Key { get; set; }
    public string Name { get; set; }
  }
}
