﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace ReleaseTracker.Jira.Models
{
  public sealed class JiraSearchResult
  {
    public int StartAt { get; set; }
    public int MaxResults { get; set; }
    public int Total { get; set; }

    public List<JiraIssue> Issues { get; set; }
  }
}
