﻿using Newtonsoft.Json;

namespace ReleaseTracker.Jira.Models
{
  public sealed class JiraStatus
  {
    public string Name { get; set; }
  }
}