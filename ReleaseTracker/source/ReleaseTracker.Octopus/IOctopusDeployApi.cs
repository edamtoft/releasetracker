﻿using System.Threading.Tasks;
using ReleaseTracker.Common;
using ReleaseTracker.Octopus.Models;

namespace ReleaseTracker.Octopus
{
  /// <summary>
  /// Represents an API for getting information on a release packages, deployments, and environments
  /// </summary>
  public interface IOctopusDeployApi
  {
    Task<DeploymentInfo> GetStatus(DomainApp app, string baselineEnvironment, string candidateEnvironment);

    Task<OctopusDeployment> GetDeployment(DomainApp app, string environment);
  }
}