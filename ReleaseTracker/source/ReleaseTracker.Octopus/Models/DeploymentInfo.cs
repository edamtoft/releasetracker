﻿using ReleaseTracker.Common;
using System;
using System.Collections.Generic;
using System.Text;

namespace ReleaseTracker.Octopus.Models
{
  public sealed class DeploymentInfo : AppInfo
  {
    public string UILink { get; set; }
    public OctopusDeployment BaselineDeployment { get; set; }
    public OctopusDeployment CandidateDeployment { get; set; }
    public bool HasChanges => !string.Equals(BaselineDeployment?.Version, CandidateDeployment?.Version, StringComparison.OrdinalIgnoreCase);
  }
}
