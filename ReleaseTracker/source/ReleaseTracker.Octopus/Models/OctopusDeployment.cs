﻿using ReleaseTracker.Common;
using System;

namespace ReleaseTracker.Octopus.Models
{
  public class OctopusDeployment : AppInfo
  {
    public string Version { get; set; }
    public string UILink { get; set; }
    public DateTimeOffset? When { get; set; }
  }
}