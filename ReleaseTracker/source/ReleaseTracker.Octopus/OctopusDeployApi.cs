﻿using Microsoft.Extensions.Options;
using Octopus.Client;
using Octopus.Client.Model;
using ReleaseTracker.Common;
using ReleaseTracker.Octopus.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace ReleaseTracker.Octopus
{
  public sealed class OctopusDeployApi : IOctopusDeployApi
  {
    private readonly OctopusServerEndpoint _endpoint;

    public OctopusDeployApi(
      IOptions<OctopusDeployOptions> options)
    {
      _endpoint = new OctopusServerEndpoint(
        options.Value.Endpoint, 
        options.Value.ApiKey);
    }

    public async Task<DeploymentInfo> GetStatus(DomainApp app, string baselineEnvironment, string candidateEnvironment)
    {
      using (var client = await OctopusAsyncClient.Create(_endpoint))
      {
        var project = await client.Repository.Projects.FindByName(app.OctopusProject);

        return new DeploymentInfo
        {
          UILink = _endpoint.OctopusServer.Resolve(project.Link("Web")).ToString(),
          App = app,
          BaselineDeployment = await GetDeployment(client.Repository, app, project, baselineEnvironment),
          CandidateDeployment = await GetDeployment(client.Repository, app, project, candidateEnvironment),
        };
      }
    }

    public async Task<OctopusDeployment> GetDeployment(DomainApp app, string environment)
    {
      using (var client = await OctopusAsyncClient.Create(_endpoint))
      {
        var project = await client.Repository.Projects.FindByName(app.OctopusProject);

        return await GetDeployment(client.Repository, app, project, environment);
      }
    }

    private async Task<OctopusDeployment> GetDeployment(IOctopusAsyncRepository repository, DomainApp app, ProjectResource project, string environmentName)
    {
      var env = await repository.Environments.FindByName(environmentName);

      if (env == null)
      {
        return null;
      }

      var deployments = await repository.Deployments.FindBy(
        projects: new[] { project.Id },
        environments: new[] { env.Id },
        skip: 0,
        take: 5);

      foreach (var deployment in deployments.Items)
      {
        var task = await repository.Tasks.Get(deployment.TaskId);

        if (task.State != TaskState.Success && task.State != TaskState.Executing)
        {
          continue;
        }

        var release = await repository.Releases.Get(deployment.ReleaseId);

        return new OctopusDeployment
        {
          App = app,
          UILink = _endpoint.OctopusServer.Resolve(release.Link("Web")).ToString(),
          Version = release.Version,
          When = task.CompletedTime,
        };
      }

      return null;
    }
  }
}
