﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ReleaseTracker.Octopus
{
  public sealed class OctopusDeployOptions
  {
    public string Endpoint { get; set; }
    public string ApiKey { get; set; }
  }
}
