﻿using System.Threading.Tasks;
using ReleaseTracker.Common;
using ReleaseTracker.TeamCity.Models;

namespace ReleaseTracker.TeamCity
{
  public interface ITeamCityApi
  {
    Task<TeamCityBuild> GetBuildByNumber(DomainApp app, string buildNumber);

    Task<TeamCityBuild> GetBuildByTag(DomainApp app, string tag);

    Task<TeamCityBuild> GetBuildBySha1(DomainApp app, string sha1);
  }
}