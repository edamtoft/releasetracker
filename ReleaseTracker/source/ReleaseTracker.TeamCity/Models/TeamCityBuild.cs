﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ReleaseTracker.TeamCity.Models
{
  public sealed class TeamCityBuild
  {
    public string BuildNumberTag { get; set; }
    public string GitCommit { get; set; }
    public string UiLink { get; set; }
    public string BuildNumber { get; set; }
  }
}
