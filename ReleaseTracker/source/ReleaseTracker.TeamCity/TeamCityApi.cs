﻿using Microsoft.Extensions.Options;
using Newtonsoft.Json.Linq;
using ReleaseTracker.Common;
using ReleaseTracker.TeamCity.Models;
using System;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;

namespace ReleaseTracker.TeamCity
{
  public sealed class TeamCityApi : ITeamCityApi
  {
    private readonly TeamCityOptions _options;
    private readonly HttpClient _client;

    public TeamCityApi(IOptions<TeamCityOptions> options)
    {
      _options = options.Value;
      _client = new HttpClient
      {
        BaseAddress = new Uri(_options.Endpoint),
        DefaultRequestHeaders =
        {
          { "Accept", "application/json" },
          { "Authorization", _options.Authorization }
        }
      };
    }

    public async Task<TeamCityBuild> GetBuildByNumber(DomainApp app, string buildNumber)
    {
      return await GetBuild(await GetBuildId(app, $"number:({buildNumber})"));
    }

    public async Task<TeamCityBuild> GetBuildByTag(DomainApp app, string tag)
    {
      return await GetBuild(await GetBuildId(app, $"tag:{tag}"));
    }

    public async Task<TeamCityBuild> GetBuildBySha1(DomainApp app, string sha1)
    {
      return await GetBuild(await GetBuildId(app, $"revision:{sha1}"));
    }

    private async Task<TeamCityBuild> GetBuild(int? buildId)
    {
      if (!buildId.HasValue)
      {
        return null;
      }

      using (var response = await _client.GetAsync($"/app/rest/builds/{buildId.Value}"))
      {
        var responseText = await response.Content.ReadAsStringAsync();

        var json = JObject.Parse(responseText);

        return new TeamCityBuild
        {
          GitCommit = json["revisions"]?["revision"].FirstOrDefault()?.Value<string>("version"),
          BuildNumberTag = json["tags"]?["tag"].FirstOrDefault()?.Value<string>("name"),
          BuildNumber = json.Value<string>("number"),
          UiLink = json.Value<string>("webUrl"),
        };
      }
    }

    private async Task<int?> GetBuildId(string buildTypeId, string locator)
    {
      using (var response = await _client.GetAsync($"/app/rest/buildTypes/id:{buildTypeId}/builds?locator=branch:default:any,{HttpUtility.UrlEncode(locator)}"))
      {
        if (!response.IsSuccessStatusCode)
        {
          return null;
        }

        var responseText = await response.Content.ReadAsStringAsync();

        var json = JObject.Parse(responseText);

        return json["build"]?.FirstOrDefault()?.Value<int>("id");
      }
    }

    private async Task<int?> GetBuildId(DomainApp app, string locator)
    {
      var buildId = await GetBuildId(app.TeamCityBuildType, locator);

      if (buildId.HasValue)
      {
        return buildId;
      }

      if (app.AlternateTeamCityBuildType != null)
      {
        return await GetBuildId(app.AlternateTeamCityBuildType, locator);
      }

      return null;
    }
  }
}
