﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ReleaseTracker.TeamCity
{
  public sealed class TeamCityOptions
  {
    public string Authorization { get; set; }
    public string Endpoint { get; set; }
  }
}
