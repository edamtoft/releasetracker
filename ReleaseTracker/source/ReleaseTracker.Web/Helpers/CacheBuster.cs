﻿using Microsoft.AspNetCore.Html;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ReleaseTracker.Web.Helpers
{
  public static class CacheBuster
  {
    public static string CacheBust(this IUrlHelper url, string resource)
    {
      return url.Content(resource + "?cachebust=" + RandomVersion);
    }

    public static int RandomVersion { get; } = new Random().Next(10000, 99999);
  }
}
