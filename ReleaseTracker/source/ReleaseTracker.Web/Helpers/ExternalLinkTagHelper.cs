﻿using Microsoft.AspNetCore.Razor.TagHelpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ReleaseTracker.Web.Helpers
{

  [HtmlTargetElement("a", Attributes = "[target='_blank']")]
  public class ExternalLinkTagHelper : TagHelper
  {
    public override void Process(TagHelperContext context, TagHelperOutput output)
    {
      output.PostContent.AppendHtml(" <i class=\"fa fa-external-link\"></i>");
    }
  }
}
