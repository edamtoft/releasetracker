﻿using Microsoft.AspNetCore.Razor.TagHelpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ReleaseTracker.Web.Helpers
{
  [HtmlTargetElement("info-popover", Attributes = "title,content")]
  public sealed class InfoPopoverTagHelper : TagHelper
  {
    public override void Process(TagHelperContext context, TagHelperOutput output)
    {
      output.TagName = "i";
      output.TagMode = TagMode.StartTagAndEndTag;
      output.Attributes.Add("class", "text-primary fa fa-info-circle");
      output.Attributes.Add("data-toggle", "popover");
      output.Attributes.Add("data-trigger", "hover");
      output.Attributes.Add("title", context.AllAttributes["title"].Value);
      output.Attributes.Add("data-content", context.AllAttributes["content"].Value);
    }
  }
}
