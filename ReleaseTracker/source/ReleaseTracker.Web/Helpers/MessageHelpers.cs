﻿using Microsoft.AspNetCore.Html;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Razor.TagHelpers;
using ReleaseTracker.Git.Models;
using ReleaseTracker.Jira.Models;
using ReleaseTracker.Web.Models;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.Encodings.Web;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web;

namespace ReleaseTracker.Web.Helpers
{
  public static class MessageHelpers
  {
    public static IHtmlContent Render(this IHtmlHelper html, FormattableString format)
    {
      var args = new object[format.ArgumentCount];

      for (var i = 0; i < format.ArgumentCount; i++)
      {
        var arg = format.GetArgument(i);
        args[i] = ResolveArgument(format.GetArgument(i));
      }

      return html.Raw(string.Format(format.Format, args));
    }

    private static string ResolveArgument(object arg)
    {
      switch (arg)
      {
        case AppChange commit:
          return Resolve(commit);
        case IEnumerable<AppChange> changes:
          return string.Join(", ", changes.Select(obj => Resolve(obj)));
        case JiraIssue issue:
          return Resolve(issue);
        case IEnumerable<JiraIssue> issues:
          return string.Join(", ", issues.Select(obj => Resolve(obj)));
        case JiraIssueRef issue:
          return Resolve(issue);
        case IHtmlContent content:
          return WriteHtml(content);
        default:
          return HttpUtility.HtmlEncode(arg);
      }
    }

    private static string WriteHtml(IHtmlContent content)
    {
      using (var writer = new StringWriter())
      {
        content.WriteTo(writer, HtmlEncoder.Default);
        return writer.ToString();
      }
    }

    private static string Resolve(AppChange commit)
    {
      var tag = new TagBuilder("a")
      {
        Attributes =
            {
              ["href"] = commit.App.GitUrl.Replace(".git", $"/commits/{commit.CommitId}"),
              ["title"] = commit.Message,
              ["target"] = "_blank",
            },
      };
      tag.InnerHtml.SetContent(commit.CommitId.Substring(0, 8));
      return WriteHtml(tag);
    }

    private static string Resolve<TFields>(JiraIssue<TFields> jiraIssue) where TFields : JiraIssueFields
    {
      var tag = new TagBuilder("a")
      {
        Attributes =
            {
              ["href"] = $"https://jira.dealeron.com/browse/{jiraIssue.Key}",
              ["title"] = jiraIssue.Fields.Summary,
              ["target"] = "_blank",
            },
      };
      tag.InnerHtml.SetContent($"{jiraIssue.Key} ({jiraIssue.Fields.Status.Name})");
      return WriteHtml(tag);
    }
  }
}
