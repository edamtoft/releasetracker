﻿using Microsoft.AspNetCore.Mvc.Rendering;
using ReleaseTracker.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ReleaseTracker.Web.Helpers
{
  public static class MessageLevelUtils
  {
    public static string ToClassName(this MessageLevel level, string prefix)
    {
      switch (level)
      {
        case MessageLevel.None:
          return prefix + "-success";
        case MessageLevel.Info:
          return prefix + "-information";
        case MessageLevel.Warning:
          return prefix + "-warning";
        case MessageLevel.Error:
        default:
          return prefix + "-danger";
      }
    }

    public static MessageLevel MaxLevel(this IEnumerable<Message> messages)
    {
      return messages.Aggregate(MessageLevel.None, (level, message) => message.Level > level ? message.Level : level);
    }
  }
}
