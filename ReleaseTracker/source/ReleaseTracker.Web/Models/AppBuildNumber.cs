﻿using ReleaseTracker.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ReleaseTracker.Web.Models
{
  public sealed class AppBuildNumber : AppInfo
  {
    public string BuildNumber { get; set; }
    public string Sha1 { get; set; }
  }
}
