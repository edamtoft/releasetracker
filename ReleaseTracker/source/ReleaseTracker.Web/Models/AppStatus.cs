﻿using ReleaseTracker.Common;
using ReleaseTracker.Octopus.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace ReleaseTracker.Web.Models
{
  public sealed class AppStatus : AppInfo
  {
    public DomainAppBuild BaselineBuild { get; set; }
    public DomainAppBuild CandidateBuild { get; set; }
    public List<IssueChanges> ChangesAhead { get; } = new List<IssueChanges>();
    public List<IssueChanges> ChangesBehind { get; } = new List<IssueChanges>();
    public List<Message> Messages { get; } = new List<Message>();
    public bool HasChanges => BaselineBuild?.GitCommit != CandidateBuild?.GitCommit;
    public bool HasGitCommits => BaselineBuild?.GitCommit != null && CandidateBuild?.GitCommit != null;
  }
}
