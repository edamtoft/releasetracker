﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ReleaseTracker.Web.Models
{
  public class BuildParameter
  {
    public string App { get; set; }
    public string BuildNumber { get; set; }
  }
}
