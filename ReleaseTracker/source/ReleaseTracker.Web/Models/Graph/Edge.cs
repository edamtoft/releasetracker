﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ReleaseTracker.Web.Models.Graph
{
  public abstract class Edge : IEquatable<Edge>
  {
    public string Source { get; set; }
    public string Target { get; set; }

    public override bool Equals(object obj) => Equals(obj as Edge);

    public bool Equals(Edge other) => other != null &&
      Source == other.Source &&
      Target == other.Target;

    public override int GetHashCode() => HashCode.Combine(Source, Target);

    public static bool operator ==(Edge edge1, Edge edge2) => EqualityComparer<Edge>.Default.Equals(edge1, edge2);

    public static bool operator !=(Edge edge1, Edge edge2) => !(edge1 == edge2);
  }
}
