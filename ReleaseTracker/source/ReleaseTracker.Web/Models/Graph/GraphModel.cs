﻿using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;
using System.Threading.Tasks;

namespace ReleaseTracker.Web.Models.Graph
{
  public sealed class GraphModel<TNode,TEdge> where TNode : Node where TEdge : Edge
  {
    public GraphModel(IEnumerable<TNode> nodes, IEnumerable<TEdge> edges)
    {
      Nodes = nodes?.ToImmutableHashSet() ?? throw new ArgumentNullException(nameof(nodes));
      Edges = edges?.ToImmutableHashSet() ?? throw new ArgumentNullException(nameof(edges));
    }

    public GraphModel(ImmutableHashSet<TNode> nodes, ImmutableHashSet<TEdge> edges)
    {
      Nodes = nodes ?? throw new ArgumentNullException(nameof(nodes));
      Edges = edges ?? throw new ArgumentNullException(nameof(edges));
    }

    public GraphModel() : this(ImmutableHashSet<TNode>.Empty, ImmutableHashSet<TEdge>.Empty)
    {
    }

    public ImmutableHashSet<TNode> Nodes { get; }
    public ImmutableHashSet<TEdge> Edges { get; }

    public bool ContainsNode(string id) => Nodes.Any(node => node.Id == id);

    public bool ContainsEdge(string source, string target) => Edges.Any(node => node.Source == source && node.Target == target);

    public static GraphModel<TNode,TEdge> operator +(GraphModel<TNode,TEdge> a, GraphModel<TNode, TEdge> b)
    {
      return new GraphModel<TNode, TEdge>(a.Nodes.Union(b.Nodes), a.Edges.Union(b.Edges));
    }
  }
}
