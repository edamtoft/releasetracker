﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ReleaseTracker.Web.Models.Graph
{
  public class JiraLink : Edge
  {
    public string Type { get; set; }
  }
}
