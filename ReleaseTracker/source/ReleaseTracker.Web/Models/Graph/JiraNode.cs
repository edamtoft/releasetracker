﻿using ReleaseTracker.Jira.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ReleaseTracker.Web.Models.Graph
{
  public class JiraNode : Node
  {
    public string Summary { get; set; }
    public string Status { get; set; }
    public string Project { get; set; }
    public double? StoryPoints { get; set; }
    public bool Highlighted { get; set; }

    public static JiraNode FromIssue(JiraIssue issue, bool highlight = false)
    {
      return new JiraNode
      {
        Id = issue.Key,
        Summary = issue.Fields.Summary,
        Status = issue.Fields.Status.Name,
        Project = issue.Fields.Project.Key,
        StoryPoints = issue.Fields.StoryPoints,
        Highlighted = highlight,
      };
    }
  }
}
