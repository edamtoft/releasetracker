﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ReleaseTracker.Web.Models.Graph
{
  public abstract class Node : IEquatable<Node>
  {
    public string Id { get; set; }

    public override bool Equals(object obj) => Equals(obj as Node);

    public bool Equals(Node other) => other != null && Id == other.Id;

    public override int GetHashCode() => HashCode.Combine(Id);

    public static bool operator ==(Node node1, Node node2) => EqualityComparer<Node>.Default.Equals(node1, node2);

    public static bool operator !=(Node node1, Node node2) => !(node1 == node2);
  }
}
