﻿using ReleaseTracker.Git.Models;
using ReleaseTracker.Jira.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ReleaseTracker.Web.Models
{
  public class IssueChanges
  {
    public JiraIssue Issue { get; set; }

    public List<AppChange> Changes { get; set; }
  }
}
