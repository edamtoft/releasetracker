﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ReleaseTracker.Jira.Models;

namespace ReleaseTracker.Web.Models
{
  public class LinkableIssue
  {
    public string Key { get; set; }
    public string Status { get; set; }
    string Title { get; set; }
    public List<string> DependsOn { get; set; }

    internal static LinkableIssue FromJiraApiModel(JiraIssue jiraIssue)
    {
      return new LinkableIssue
      {
        Key = jiraIssue.Key,
        Title = jiraIssue.Fields.Summary,
        Status = jiraIssue.Fields.Status.Name,
        DependsOn = jiraIssue.Dependencies.Select(issue => issue.Key).ToList()
      };
    }
  }
}
