﻿using Microsoft.AspNetCore.Html;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ReleaseTracker.Web.Models
{
  public class Message
  {
    public MessageLevel Level { get; set; } = MessageLevel.Error;
    public string Title { get; set; }
    public FormattableString Description { get; set; }
  }
}
