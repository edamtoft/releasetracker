﻿namespace ReleaseTracker.Web.Models
{
  public enum MessageLevel
  {
    None = 0,
    Info = 1,
    Warning = 2,
    Error = 3,
  }
}