﻿using ReleaseTracker.Git.Models;
using ReleaseTracker.Jira.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ReleaseTracker.Web.Models
{
  public class ReleaseCandidate
  {
    public string BaselineEnvironment { get; set; }
    public string CandidateEnvironment { get; set; }

    public List<AppStatus> Apps { get; set; }

    public IEnumerable<Message> Messages => Apps.SelectMany(app => app.Messages);
    public IEnumerable<IssueChanges> Changes => Apps.SelectMany(app => app.ChangesAhead);
  }
}
