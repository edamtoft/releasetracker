﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ReleaseTracker.Web.Models
{
  public enum WorkflowState
  {
    Dev,
    Qa,
    Uat,
    Done,
  }
}
