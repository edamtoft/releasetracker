﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using ReleaseTracker.Common;
using ReleaseTracker.Web.Models;
using ReleaseTracker.Web.Services;

namespace ReleaseTracker.Web.Pages
{
  public class BuildsModel : PageModel
  {
    private readonly IAppIndex _apps;
    private readonly IBuildHealthService _buildHealth;

    public BuildsModel(IAppIndex apps, IBuildHealthService buildHealth)
    {
      _apps = apps;
      _buildHealth = buildHealth;
    }

    public List<AppStatus> Builds { get; private set; }
    public DomainApp App { get; private set; }


    public async Task<IActionResult> OnGet(string app)
    {
      if (!_apps.TryGetApp(app, out var domainApp))
      {
        return NotFound();
      }

      App = domainApp;
      Builds =  await _buildHealth.GetUpcomingBuilds(domainApp, "Production");

      return Page();
    }
  }
}