﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using ReleaseTracker.Common;
using ReleaseTracker.Web.Models;
using ReleaseTracker.Web.Services;

namespace ReleaseTracker.Web.Pages
{
  public class CandidateModel : PageModel
  {
    private readonly IAppIndex _apps;
    private readonly IReleaseCandidateService _rcService;
    private readonly IBuildIndex _builds;

    public CandidateModel(IAppIndex apps, IReleaseCandidateService rcService, IBuildIndex builds)
    {
      _apps = apps;
      _rcService = rcService;
      _builds = builds;
    }


    [BindProperty(SupportsGet = true, Name = "builds")]
    public Dictionary<string, string> Builds { get; } = new Dictionary<string, string>(StringComparer.OrdinalIgnoreCase);

    public ReleaseCandidate RC { get; private set; }

    public Dictionary<string, SelectList> AvailableBuilds { get; } = new Dictionary<string, SelectList>();

    public async Task<IActionResult> OnGet()
    { 
      foreach (var app in _apps.GetAll())
      {
        if (!Builds.ContainsKey(app.Name))
        {
          Builds[app.Name] = null;
        }
        AvailableBuilds[app.Name] = new SelectList(
          (await _builds.GetAvailableBuilds(app)).Prepend(new AppBuildNumber { App = app, BuildNumber = null }),
          nameof(AppBuildNumber.BuildNumber),
          nameof(AppBuildNumber.BuildNumber),
          Builds[app.Name]);
      }

      if (Builds.Values.Any(build => build != null))
      {
        RC = await _rcService.Compare("Production", EnumerateBuilds());
      }
      
      return Page();
    }

    private IEnumerable<(DomainApp App, string BuildNumber)> EnumerateBuilds()
    {
      foreach (var appBuild in Builds)
      {
        if (!_apps.TryGetApp(appBuild.Key, out var app))
        {
          continue;
        }

        if (string.IsNullOrEmpty(appBuild.Value))
        {
          continue;
        }

        yield return (app, appBuild.Value);
      }
    }
  }
}