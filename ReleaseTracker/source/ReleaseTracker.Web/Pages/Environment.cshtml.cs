﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using ReleaseTracker.Common;
using ReleaseTracker.Web.Models;
using ReleaseTracker.Web.Services;

namespace ReleaseTracker.Web.Pages
{
  public class UatModel : PageModel
  {

    private readonly IReleaseCandidateService _rcService;
    private readonly IAppIndex _apps;

    public UatModel(IReleaseCandidateService rcService, IAppIndex apps)
    {
      _rcService = rcService;
      _apps = apps;
    }

    public ReleaseCandidate RC { get; private set; }

    [BindProperty(SupportsGet = true, Name = "app")]
    public string[] Apps { get; set; } = new string[0];

    [BindProperty(SupportsGet = true)]
    public string Baseline { get; set; } = "Production";

    public async Task<IActionResult> OnGet(string environment)
    {
      RC = await _rcService.Compare(Baseline, environment, FilterApps());
      return Page();
    }

    private IEnumerable<DomainApp> FilterApps()
    {
      if (Apps == null || Apps.Length == 0)
      {
        foreach (var app in _apps.GetAll())
        {
          yield return app;
        }
      }
      else
      {
        foreach (var appName in Apps)
        {
          if (_apps.TryGetApp(appName, out var app))
          {
            yield return app;
          }
        }
      }
    }
  }
}