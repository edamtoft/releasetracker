﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using ReleaseTracker.Common;
using ReleaseTracker.Web.Services;

namespace ReleaseTracker.Web.Pages
{
  public class IndexModel : PageModel
  {
    private readonly IAppIndex _apps;
    private readonly IBuildInfoService _builds;

    public IndexModel(IAppIndex apps, IBuildInfoService builds)
    {
      _apps = apps;
      _builds = builds;
    }

    public DomainAppBuild[] Apps { get; private set; }

    public async Task OnGet()
    {
      Apps = await Task.WhenAll(_apps.GetAll().Select(app => _builds.GetDomainAppBuildByEnvironment(app, "Production")));
    }
  }
}