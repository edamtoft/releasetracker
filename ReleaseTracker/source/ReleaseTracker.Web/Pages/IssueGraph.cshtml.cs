﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using ReleaseTracker.Common;
using ReleaseTracker.Web.Models;
using ReleaseTracker.Web.Models.Graph;
using ReleaseTracker.Web.Services;

namespace ReleaseTracker.Web.Pages
{
  public class IssueGraphModel : PageModel
  {
    private readonly IGraphService _graphService;

    public IssueGraphModel(IGraphService graphService)
    {
      _graphService = graphService;
    }


    [BindProperty(SupportsGet = true)]
    public string Jql { get; set; } = "status in (\"Pending UAT\")";

    [BindProperty(SupportsGet = true)]
    public bool InwardOnly { get; set; } = false;

    public GraphModel<JiraNode,JiraLink> Graph { get; private set; }

    public Dictionary<string, SelectList> AvailableBuilds { get; } = new Dictionary<string, SelectList>();
    public bool HasError { get; private set; }

    public async Task<IActionResult> OnGet()
    {
      try
      {
        Graph = await _graphService.ExpandFromSearch(Jql, InwardOnly, HttpContext.RequestAborted);
      }
      catch (Exception)
      {
        Graph = new GraphModel<JiraNode, JiraLink>();
        HasError = true;
      } 
      return Page();
    }
  }
}