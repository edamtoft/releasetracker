﻿using ReleaseTracker.Common;
using ReleaseTracker.Git;
using ReleaseTracker.Git.Models;
using ReleaseTracker.Jira;
using ReleaseTracker.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace ReleaseTracker.Web.Services
{
  public sealed class BuildComparer : IBuildComparer
  {
    private readonly IJiraApi _jira;
    private readonly ISourceRepo _repos;
    private readonly IReleaseValidator _validator;

    public BuildComparer(IJiraApi jira, ISourceRepo repos, IReleaseValidator validator)
    {
      _jira = jira;
      _repos = repos;
      _validator = validator;
    }

    public async Task<AppStatus> CompareBuilds(DomainApp app, DomainAppBuild baselineBuild, DomainAppBuild candidateBuild, WorkflowState expectedState)
    {
      var status = new AppStatus
      {
        App = app,
        BaselineBuild = baselineBuild,
        CandidateBuild = candidateBuild,
      };

      if (status.BaselineBuild?.GitCommit == null || status.CandidateBuild?.GitCommit == null)
      {
        status.Messages.Add(new Message
        {
          Level = MessageLevel.Error,
          Title = "Unable to diff builds",
          Description = $"The system was unable to associate one or both builds with a git commit. There may be missing changes.",
        });

        return status;
      }

      try
      {
        if (status.BaselineBuild?.GitCommit != status.CandidateBuild?.GitCommit)
        {
          var changesAhead = (await _repos.ListChanges(app, status.BaselineBuild.GitCommit, status.CandidateBuild.GitCommit, includeMerges: false)).ToList();
          var changesBehind = (await _repos.ListChanges(app, status.CandidateBuild.GitCommit, status.BaselineBuild.GitCommit, includeMerges: false)).ToList();

          var cherryPicks = (
            from behind in changesBehind
            let match = Regex.Match(behind.Message, @"\(cherry picked from commit ([0-9a-f]{40})\)")
            where match.Success
            join ahead in changesAhead on match.Groups[1].Value equals ahead.CommitId
            select new { ahead, behind }).ToArray();

          foreach (var cherryPick in cherryPicks)
          {
            changesAhead.Remove(cherryPick.ahead);
            changesBehind.Remove(cherryPick.behind);
          }


          status.ChangesAhead.AddRange(await AssociateJiraIssue(changesAhead));
          status.ChangesBehind.AddRange(await AssociateJiraIssue(changesBehind));
          _validator.Validate(status, expectedState);
        }
      }
      catch (Exception err)
      {
        status.Messages.Add(new Message
        {
          Level = MessageLevel.Error,
          Title = "Unable to diff builds",
          Description = $"An error occurred while diffing builds: {err.Message}"
        });
      }

      return status;
    }

    private async Task<List<IssueChanges>> AssociateJiraIssue(IEnumerable<AppChange> changes)
    {
      var tasks = changes.GroupBy(change =>
      {
        var match = Regex.Match(change.Message, @"([A-Z]{2,7}\-\d+)[\-:;\s](.*)$", RegexOptions.Multiline);

        return match.Success ? match.Groups[1].Value : null;
      }).Select(async group => new IssueChanges
      {
        Issue = group.Key == null ? null : await _jira.GetIssue(group.Key),
        Changes = group.ToList()
      });

      return (await Task.WhenAll(tasks)).ToList();
    }
  }
}
