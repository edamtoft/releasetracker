﻿using ReleaseTracker.Common;
using ReleaseTracker.Git;
using ReleaseTracker.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ReleaseTracker.Web.Services
{
  public sealed class BuildHealthService : IBuildHealthService
  {
    private readonly IBuildComparer _buildComparer;
    private readonly IBuildInfoService _buildInfo;
    private readonly IBuildIndex _builds;
    private readonly ISourceRepo _repo;

    public BuildHealthService(IBuildComparer buildComparer, IBuildInfoService buildInfo, IAppIndex apps, IBuildIndex builds, ISourceRepo repo)
    {
      _buildComparer = buildComparer;
      _buildInfo = buildInfo;
      _builds = builds;
      _repo = repo;
    }

    public async Task<List<AppStatus>> GetUpcomingBuilds(DomainApp app, string environment)
    {
      var baselineBuild = await _buildInfo.GetDomainAppBuildByEnvironment(app, environment);
      
      if (baselineBuild?.GitCommit == null)
      {
        return new List<AppStatus>();
      }

      var buildNumbers = await _builds.GetAvailableBuilds(app);

      var commits = await _repo.ListChangesFromMaster(app, baselineBuild.GitCommit, includeMerges: true);

      var upcomingBuilds = from commit in commits
                           join buildNumber in buildNumbers on commit.CommitId equals buildNumber.Sha1
                           select buildNumber;

      var statuses = await Task.WhenAll(upcomingBuilds.Select(async buildNumber =>
      {
        var upcomingBuild = await _buildInfo.GetDomainAppBuildByNumber(app, buildNumber.BuildNumber);

        return await _buildComparer.CompareBuilds(app, baselineBuild, upcomingBuild, WorkflowState.Uat);
      }));

      return statuses.ToList();
    }
  }
}
