﻿using ReleaseTracker.Common;
using ReleaseTracker.Git;
using ReleaseTracker.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ReleaseTracker.Web.Services
{
  public sealed class BuildIndex : IBuildIndex
  {
    private readonly ISourceRepo _repo;

    public BuildIndex(ISourceRepo repo)
    {
      _repo = repo;
    }

    public async Task<List<AppBuildNumber>> GetAvailableBuilds(DomainApp app)
    {
      var tags = await _repo.LookupTags(app);

      return tags
        .Where(t => t.Name.StartsWith(TagConventions.Prefix))
        .Select(tag => new AppBuildNumber
        {
          App = app,
          BuildNumber = tag.Name.Substring(TagConventions.Prefix.Length),
          Sha1 = tag.Sha1
        })
        .ToList();
    }
  }
}
