﻿using ReleaseTracker.Common;
using ReleaseTracker.Git;
using ReleaseTracker.Jira;
using ReleaseTracker.Octopus;
using ReleaseTracker.Octopus.Models;
using ReleaseTracker.TeamCity;
using ReleaseTracker.TeamCity.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace ReleaseTracker.Web.Services
{
  public sealed class BuildInfoService : IBuildInfoService
  {
    private readonly IOctopusDeployApi _octopus;
    private readonly IJiraApi _jia;
    private readonly ITeamCityApi _teamCity;
    private readonly ISourceRepo _sourceRepo;

    public BuildInfoService(IOctopusDeployApi octopus, IJiraApi jira, ITeamCityApi teamCity, ISourceRepo sourceRepo)
    {
      _octopus = octopus;
      _jia = jira;
      _teamCity = teamCity;
      _sourceRepo = sourceRepo;
    }

    public async Task<DomainAppBuild> GetDomainAppBuildByEnvironment(DomainApp app, string environment)
    {
      var build = new DomainAppBuild
      {
        App = app,
      };

      var octopusDeployment = await _octopus.GetDeployment(app, environment);

      if (octopusDeployment == null)
      {
        return build;
      }

      build.OctopusDeployUrl = octopusDeployment.UILink;
      build.ReleasedAt = octopusDeployment.When;

      var teamCityBuild = await TrySequentially(
          octopusDeployment?.Version,
          (@"^(\d+)\-([0-9a-f]{40})$", match => _teamCity.GetBuildByNumber(app, match.Value)),
          (@"^(\d+)\-([0-9a-f]{40})\-([A-Za-z]+)\-([A-Z]+\.\d+)$", match => _teamCity.GetBuildByNumber(app, match.Result("$1"))),
          (@"^(\d+)\-([0-9a-f]{40})\-([A-Za-z]+)$", match => _teamCity.GetBuildByNumber(app, match.Result("$1-$3"))),
          (@"^(\d+\.\d+\.\d+)\+([A-Z]+)\.(\d+)$", match => _teamCity.GetBuildByNumber(app, match.Result("$2.$3"))),
          (@"^(\d+)\-([0-9a-f]{40})\-([a-z]+)-(v\d+\.\d+\.\d+)\+([A-Z]+\.\d+)$", match => _teamCity.GetBuildByNumber(app, match.Result("$3, $1-$2"))),
          (@"^(\d+)\-([0-9a-f]{40})\-([a-z]+)$", match => _teamCity.GetBuildByNumber(app, match.Result("$3, $1-$2"))),
          (@"^(\d+)\-([0-9a-f]{40})\-([a-z]+)$", match => _teamCity.GetBuildByNumber(app, match.Result($"{app.TorqueEnvironment}-$3, $1-$2"))),
          (@"^(\d+\.\d+\.\d+)\+(\d+\.\d+)$", Match => _teamCity.GetBuildByNumber(app, Match.Result("$2"))),
          (@"^(\d+\.\d+\.\d+)\+([A-Z]+)\.(\d+)-", Match => _teamCity.GetBuildByNumber(app, Match.Result("$2.$3"))),
          (@"[0-9a-f]{40}", match => _teamCity.GetBuildBySha1(app, match.Value)));

      if (teamCityBuild != null)
      {
        build.TeamCityUrl = teamCityBuild.UiLink;
        build.GitCommit = teamCityBuild.GitCommit;
      }

      build.GitCommit = ResolveGitCommit(octopusDeployment, teamCityBuild);

      build.BuildNumber = await ResolveBuildNumber(app, build.GitCommit, teamCityBuild);

      return build;
    }

    public async Task<DomainAppBuild> GetDomainAppBuildByNumber(DomainApp app, string buildNumber)
    {
      var build = new DomainAppBuild
      {
        App = app,
        BuildNumber = buildNumber,
      };

      if (buildNumber == null)
      {
        return build;
      }

      var teamCityBuild = await _teamCity.GetBuildByTag(app, buildNumber) ?? await _teamCity.GetBuildByNumber(app, buildNumber);

      if (teamCityBuild != null)
      {
        build.TeamCityUrl = teamCityBuild.UiLink;
        build.GitCommit = teamCityBuild.GitCommit;
      }
      else
      {
        build.GitCommit = await _sourceRepo.GetCommitByTag(app, TagConventions.Prefix + buildNumber);
      }

      return build.GitCommit == null ? null : build;
    }

    private static string ResolveGitCommit(OctopusDeployment deployment, TeamCityBuild build)
    {
      if (build?.GitCommit != null)
      {
        return build.GitCommit;
      }

      var match = Regex.Match(deployment.Version, @"[0-9a-f]{40}");

      return match.Success ? match.Value : null;
    }

    private async Task<string> ResolveBuildNumber(DomainApp app, string gitCommit, TeamCityBuild teamCityBuild)
    {
      var gitTag = gitCommit == null
        ? null
        : (await _sourceRepo.LookupTags(app, gitCommit))
            .FirstOrDefault(tag => tag.Name.StartsWith(TagConventions.Prefix))
            .Name?.Substring(TagConventions.Prefix.Length);

      return gitTag
        ?? TryGetBuildNumber(teamCityBuild)
        ?? gitCommit?.Substring(0,6)
        ?? "UNKNOWN";
    }

    private static string TryGetBuildNumber(TeamCityBuild build)
    {
      if (build == null)
      {
        return null;
      }

      if (build.BuildNumberTag != null)
      {
        return build.BuildNumberTag;
      }

      var match = Regex.Match(build.BuildNumber, @"\b\w+\.\d+\b");

      if (match.Success)
      {
        return match.Value;
      }

      return null;
    }
       
    private static async Task<TeamCityBuild> TrySequentially(string version, params (string Pattern, Func<Match,Task<TeamCityBuild>> Function)[] patterns)
    {
      if (version == null)
      {
        return null;
      }

      foreach (var (pattern, action) in patterns)
      {
        var match = Regex.Match(version, pattern);

        if (!match.Success)
        {
          continue;
        }

        var result = await action(match);

        if (result != null)
        {
          return result;
        }
      }

      return null;
    }
  }
}
