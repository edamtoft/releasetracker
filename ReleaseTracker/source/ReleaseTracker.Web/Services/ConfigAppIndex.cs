﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Options;
using ReleaseTracker.Common;

namespace ReleaseTracker.Web.Services
{
  public class ConfigAppIndex : IAppIndex
  {
    private readonly DomainOptions _domain;

    public ConfigAppIndex(IOptions<DomainOptions> options)
    {
      _domain = options.Value;
    }

    public IEnumerable<DomainApp> GetAll()
    {
      return _domain.DomainApps;
    }

    public bool TryGetApp(string name, out DomainApp app)
    {
      foreach (var domainApp in _domain.DomainApps)
      {
        if (string.Equals(domainApp.Name, name, StringComparison.OrdinalIgnoreCase))
        {
          app = domainApp;
          return true;
        }
      }

      app = null;
      return false;
    }
  }
}
