﻿using ReleaseTracker.Jira;
using ReleaseTracker.Jira.Models;
using ReleaseTracker.Web.Models;
using ReleaseTracker.Web.Models.Graph;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace ReleaseTracker.Web.Services
{
  public sealed class GraphService : IGraphService
  {
    private readonly IJiraApi _jira;

    public GraphService(IJiraApi jira)
    {
      _jira = jira;
    }

    public async Task<GraphModel<JiraNode,JiraLink>> ExpandFromSearch(string jql, bool inwardOnly, CancellationToken ct)
    {
      ct.ThrowIfCancellationRequested();

      var issues = new ConcurrentDictionary<string, JiraIssue>();
      var links = new ConcurrentBag<JiraLink>();

      var results = await _jira.Search(jql);

      foreach (var result in results.Issues)
      {
        await VisitDependencies(result, issues, links, inwardOnly, ct);
      }

      var initialIssueKeys = new HashSet<string>(results.Issues.Select(issue => issue.Key));

      return new GraphModel<JiraNode, JiraLink>(
        issues.Values.Select(issue => JiraNode.FromIssue(issue, initialIssueKeys.Contains(issue.Key))), 
        links);
    }

    private async Task VisitDependencies(JiraIssue source, ConcurrentDictionary<string, JiraIssue> issues, ConcurrentBag<JiraLink> links, bool inwardOnly, CancellationToken ct)
    {
      ct.ThrowIfCancellationRequested();

      if (!issues.TryAdd(source.Key, source))
      {
        return;
      }

      var dependencies = source.Fields.IssueLinks
        .Select(ResolveLink)
        .Where(link => (!inwardOnly || link.Inward) && (link.Type == "Dependency" || link.Type == "Blocks"));

      foreach (var dependency in dependencies)
      {
        links.Add(dependency.Inward
          ? new JiraLink { Source = dependency.Key, Target = source.Key, Type = dependency.Type }
          : new JiraLink { Source = source.Key, Target = dependency.Key, Type = dependency.Type });

        if (issues.ContainsKey(dependency.Key))
        {
          continue;
        }

        var linkedIssue = await _jira.GetIssue(dependency.Key);

        await VisitDependencies(linkedIssue, issues, links, inwardOnly, ct);
      }
    }

    private static (string Type, string Key, bool Inward) ResolveLink(JiraIssueLink link)
    {
      if (link.InwardIssue != null)
      {
        return (link.Type.Name, link.InwardIssue.Key, true);
      }
      else if (link.OutwardIssue != null)
      {
        return (link.Type.Name, link.OutwardIssue.Key, false);
      }
      throw new ArgumentException("Unable to resolve link");
    }
  }
}
