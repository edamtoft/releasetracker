﻿using ReleaseTracker.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ReleaseTracker.Web.Services
{
  public interface IAppIndex
  {
    bool TryGetApp(string name, out DomainApp app);

    IEnumerable<DomainApp> GetAll();
  }
}
