﻿using System.Threading.Tasks;
using ReleaseTracker.Common;
using ReleaseTracker.Web.Models;

namespace ReleaseTracker.Web.Services
{
  public interface IBuildComparer
  {
    Task<AppStatus> CompareBuilds(DomainApp app, DomainAppBuild baselineBuild, DomainAppBuild candidateBuild, WorkflowState expectedState);
  }
}