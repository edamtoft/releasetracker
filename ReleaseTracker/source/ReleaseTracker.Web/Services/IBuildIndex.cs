﻿using System.Collections.Generic;
using System.Threading.Tasks;
using ReleaseTracker.Common;
using ReleaseTracker.Web.Models;

namespace ReleaseTracker.Web.Services
{
  public interface IBuildIndex
  {
    Task<List<AppBuildNumber>> GetAvailableBuilds(DomainApp app);
  }
}