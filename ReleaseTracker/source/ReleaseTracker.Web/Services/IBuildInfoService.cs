﻿using System.Threading.Tasks;
using ReleaseTracker.Common;

namespace ReleaseTracker.Web.Services
{
  public interface IBuildInfoService
  {
    Task<DomainAppBuild> GetDomainAppBuildByEnvironment(DomainApp app, string environment);
    Task<DomainAppBuild> GetDomainAppBuildByNumber(DomainApp app, string buildNumber);
  }
}