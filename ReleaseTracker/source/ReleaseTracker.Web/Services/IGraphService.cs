﻿using System.Collections.Concurrent;
using System.Threading;
using System.Threading.Tasks;
using ReleaseTracker.Web.Models.Graph;

namespace ReleaseTracker.Web.Services
{
  public interface IGraphService
  {
    Task<GraphModel<JiraNode, JiraLink>> ExpandFromSearch(string key, bool inwardOnly, CancellationToken ct);
  }
}