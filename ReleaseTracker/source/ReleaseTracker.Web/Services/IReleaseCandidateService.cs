﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Primitives;
using ReleaseTracker.Common;
using ReleaseTracker.Web.Models;

namespace ReleaseTracker.Web.Services
{
  /// <summary>
  /// Represents the domain-logic for associating environments, releases, issues, and git history
  /// </summary>
  public interface IReleaseCandidateService
  {
    Task<ReleaseCandidate> Compare(string baselineEnvironment, string candidateEnvironment, IEnumerable<DomainApp> apps);
    Task<ReleaseCandidate> Compare(string baselineEnvironment, IEnumerable<(DomainApp App, string BuildNumber)> builds);
  }
}