﻿using ReleaseTracker.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ReleaseTracker.Web.Services
{
  public interface IReleaseValidator
  {
    void Validate(AppStatus status, WorkflowState expectedState);
  }
}
