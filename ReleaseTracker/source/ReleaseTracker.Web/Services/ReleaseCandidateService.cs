﻿using Microsoft.AspNetCore.Html;
using Microsoft.Extensions.Options;
using Microsoft.Extensions.Primitives;
using ReleaseTracker.Common;
using ReleaseTracker.Git;
using ReleaseTracker.Git.Models;
using ReleaseTracker.Jira;
using ReleaseTracker.Octopus;
using ReleaseTracker.Octopus.Models;
using ReleaseTracker.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web;

namespace ReleaseTracker.Web.Services
{
  public sealed class ReleaseCandidateService : IReleaseCandidateService
  {
    private readonly IBuildInfoService _buildInfo;
    private readonly IBuildComparer _buildComparer;

    public ReleaseCandidateService(IBuildInfoService buildInfo, IBuildComparer buildComparer)
    {
      _buildInfo = buildInfo;
      _buildComparer = buildComparer;
    }

    public async Task<ReleaseCandidate> Compare(string baselineEnvironment, string candidateEnvironment, IEnumerable<DomainApp> apps)
    {
      var appStatuses = await Task.WhenAll(apps.Select(async app => 
      {
        var baselineBuild = await _buildInfo.GetDomainAppBuildByEnvironment(app, baselineEnvironment);
        var candidateBuild = await _buildInfo.GetDomainAppBuildByEnvironment(app, candidateEnvironment);

        return await _buildComparer.CompareBuilds(app, baselineBuild, candidateBuild, EnvironmentState(candidateEnvironment));
      }));

      return new ReleaseCandidate
      {
        Apps = appStatuses.ToList(),
        BaselineEnvironment = baselineEnvironment,
        CandidateEnvironment = candidateEnvironment,
      };
    }

    public async Task<ReleaseCandidate> Compare(string baselineEnvironment, IEnumerable<(DomainApp App, string BuildNumber)> builds)
    {

      var appStatuses = await Task.WhenAll(builds.Select(async appBuild =>
      {
        var (app, buildNumber) = appBuild;

        var baselineBuild = await _buildInfo.GetDomainAppBuildByEnvironment(app, baselineEnvironment);

        var candidateBuild = await _buildInfo.GetDomainAppBuildByNumber(app, buildNumber);

        return await _buildComparer.CompareBuilds(app, baselineBuild, candidateBuild, WorkflowState.Uat);
      }));

      return new ReleaseCandidate
      {
        Apps = appStatuses.ToList(),
        BaselineEnvironment = baselineEnvironment,
        CandidateEnvironment = "Candidate",
      };
    }

    private static WorkflowState EnvironmentState(string environmentName)
    {
      switch (environmentName?.ToLower())
      {
        case "uat":
          return WorkflowState.Uat;
        case "qa":
          return WorkflowState.Qa;
        default:
          return WorkflowState.Dev;
      }
    }
  }
}
