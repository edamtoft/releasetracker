﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ReleaseTracker.Web.Models;

namespace ReleaseTracker.Web.Services
{
  public class ReleaseValidator : IReleaseValidator
  {
    public void Validate(AppStatus status, WorkflowState expectedState)
    {
      if (status.CandidateBuild?.TeamCityUrl == null)
      {
        status.Messages.Add(new Message
        {
          Level = MessageLevel.Warning,
          Title = "Unable To Find TeamCity Build",
          Description = $"This may be a linking problem, but it may also indicate that an app was build in an unexpected configuration."
        });
      }

      foreach (var group in status.ChangesAhead.Where(group => group.Issue != null && GetWorkflowStatus(group.Issue.Fields.Status.Name) < expectedState))
      {
        status.Messages.Add(new Message
        {
          Level = MessageLevel.Warning,
          Title = $"{group.Issue.Key} Is {group.Issue.Fields.Status.Name}",
          Description = $"{group.Issue} is not yet marked as being ready for UAT / Release"
        });
      }

      foreach (var group in status.ChangesAhead.Where(group => group.Issue == null))
      {
        status.Messages.Add(new Message
        {
          Level = MessageLevel.Warning,
          Title = $"Unassociated Commits",
          Description = $"Commits {group.Changes} cannot be linked to a JIRA ticket. This prevents issue status and dependencies from being tracked."
        });
      }

      foreach (var (ticket, dependency) in
        from change in status.ChangesAhead
        where change.Issue != null
        let ticket = change.Issue
        from dependency in change.Issue.Dependencies
        where GetWorkflowStatus(dependency.Fields.Status.Name) < GetWorkflowStatus(ticket.Fields.Status.Name)
        select (ticket, dependency))
      {
        status.Messages.Add(new Message
        {
          Level = MessageLevel.Warning,
          Title = $"Unmet Dependency For {ticket.Key}",
          Description = $"{ticket} depends on {dependency}",
        });
      }

      if (status.ChangesBehind.Any())
      {
        var tickets = status.ChangesBehind.Where(group => group.Issue != null).Select(group => group.Issue);

        var commits = status.ChangesBehind.SelectMany(group => group.Changes);

        status.Messages.Add(new Message
        {
          Level = MessageLevel.Error,
          Title = $"Build {status.CandidateBuild.BuildNumber} Is Behind {status.BaselineBuild.BuildNumber}",
          Description = $"Code associated with ticket(s) {tickets} are missing in this build. Commits: {commits}"
        });
      }
    }

    private static WorkflowState GetWorkflowStatus(string status)
    {
      switch (status)
      {
        case "Pending Deploy":
        case "Pending QA":
          return WorkflowState.Qa;
        case "Pending UAT":
        case "Pending Promotion":
          return WorkflowState.Uat;
        case "Closed":
        case "Released":
        case "Pending Confirmation":
          return WorkflowState.Done;
        case "In Progress":
        case "Pending Build":
        case "Scoped":
        case "New":
        default:
          return WorkflowState.Dev;
      }
    }
  }
}
