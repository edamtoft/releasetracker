﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using ReleaseTracker.Common;
using ReleaseTracker.Git;
using ReleaseTracker.Jira;
using ReleaseTracker.Octopus;
using ReleaseTracker.TeamCity;
using ReleaseTracker.Web.Services;

namespace ReleaseTracker.Web
{
  public class Startup
  {
    public Startup(IConfiguration configuration)
    {
      Configuration = configuration;
    }

    public IConfiguration Configuration { get; }

    public void ConfigureServices(IServiceCollection services)
    {
      services.AddMvc();

      services.Configure<JiraOptions>(Configuration.GetSection("Jira"));
      services.Configure<OctopusDeployOptions>(Configuration.GetSection("Octopus"));
      services.Configure<DomainOptions>(Configuration.GetSection("Domain"));
      services.Configure<TeamCityOptions>(Configuration.GetSection("TeamCity"));
      services.Configure<BitbucketApiOptions>(Configuration.GetSection("Bitbucket"));

      services.AddSingleton<ISourceRepo, BitbucketApi>();
      services.AddScoped<IAppIndex, ConfigAppIndex>();
      services.AddScoped<IOctopusDeployApi, OctopusDeployApi>();
      services.AddSingleton<IJiraApi, JiraApi>();
      services.AddSingleton<ITeamCityApi, TeamCityApi>();
      services.AddScoped<IReleaseCandidateService, ReleaseCandidateService>();
      services.AddScoped<IBuildComparer, BuildComparer>();
      services.AddScoped<IBuildIndex, BuildIndex>();
      services.AddScoped<IBuildHealthService, BuildHealthService>();
      services.AddScoped<IBuildInfoService, BuildInfoService>();
      services.AddScoped<IGraphService, GraphService>();
      services.AddScoped<IReleaseValidator, ReleaseValidator>();
    }

    public void Configure(IApplicationBuilder app, IHostingEnvironment env)
    {
      app.UseDeveloperExceptionPage();
      app.UseStaticFiles();

      app.UseMvcWithDefaultRoute();
    }
  }
}
