﻿(function () {
  const svg = d3.select("#visualization");
  const { height, width } = svg.node().getBoundingClientRect();

  const view = svg.select("g#view");

  svg.call(d3
    .zoom()
    .on("zoom", () => view.attr("transform", d3.event.transform)));

  const graph = JSON.parse(document.getElementById("graph-data").textContent);

  function radius(issue) {
    const relative = Math.sqrt((issue.storyPoints || 1) / Math.PI);
    return relative * 120;
  }

  const simulation = d3.forceSimulation(graph.nodes)
    .force("link", d3.forceLink(graph.edges).id(d => d.id).distance(d => (radius(d.source) + radius(d.target)) * 1.5))
    .force("collide", d3.forceCollide(d => radius(d) + 20).iterations(10))
    .force("charge", d3.forceManyBody().distanceMax(250))
    .force("center", d3.forceCenter(width / 2, height / 2));

  const edges = view
    .append("g")
    .attr("class", "edges")
    .selectAll("line")
    .data(graph.edges)
    .enter()
    .append("line")
    .attr("class", "jira-link")
    .attr("data-link-type", d => d.type);

  const nodes = view
    .append("g")
    .attr("class", "nodes")
    .selectAll("g")
    .data(graph.nodes)
    .enter()
    .append("g")
    .attr("class", "jira-issue")
    .attr("data-status", d => d.status)
    .attr("data-project", d => d.project)
    .attr("data-domain", d => d.project.length === 7 ? d.project.substring(0, 3) : undefined)
    .attr("data-highlighted", d => d.highlighted)
    .call(d3.drag()
      .on("start", d => {
        if (!d3.event.active) simulation.alphaTarget(0.3).restart();
        d.fx = d.x;
        d.fy = d.y;
      })
      .on("drag", d => {
        d.fx = d3.event.x;
        d.fy = d3.event.y;
      })
      .on("end", d => {
        if (!d3.event.active) simulation.alphaTarget(0);
        d.fx = null;
        d.fy = null;
      }));

  nodes
    .append("circle")
    .attr("r", radius);

  const nodeText = nodes
    .append("a")
    .attr("href", d => `https://jira.dealeron.com/browse/${d.id}`)
    .attr("target", "_blank")
    .append("text")
    .attr("text-anchor", "middle")
    .attr("alignment-baseline", "middle");

  nodeText
    .append("tspan")
    .attr("x", 0)
    .text(d => d.id);

  nodeText
    .append("tspan")
    .attr("x", 0)
    .attr("dy", "1.2em")
    .text(d => d.status);

  nodes
    .append("title")
    .text(d => d.summary);


  function getOffset(d) {
    var r = radius(d.target);
    var dx = d.target.x - d.source.x;
    var dy = d.target.y - d.source.y;
    var gamma = Math.atan2(dy, dx);
    var x = d.target.x - Math.cos(gamma) * r;
    var y = d.target.y - Math.sin(gamma) * r;
    return { x, y };
  }

  simulation.on("tick", () => {
    edges
      .attr("x1", d => d.source.x)
      .attr("y1", d => d.source.y)
      .attr("x2", d => getOffset(d).x)
      .attr("y2", d => getOffset(d).y);

    nodes
      .attr("transform", d => `translate(${d.x},${d.y})`);
  });
})();